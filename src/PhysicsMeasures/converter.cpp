#include "converter.h"

Converter::Converter() :
    m_type("Massa"), m_types { "Massa", "Temperature", "Volume" }
{

}

void Converter::setType(const QString& _type)
{
    // Check for valid Converter type
    if (!m_types.contains(_type))
        return;

    if (_type != m_type) {
        m_type = _type;
        emit typeChanged();
    }
}

