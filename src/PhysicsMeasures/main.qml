import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0

import com.h05t.qml.converter 1.0   // new component Converter
import com.h05t.qml.mass 1.0        // new component MassValue
import com.h05t.qml.temp 1.0        // new component TemperatureValue
import com.h05t.qml.volume 1.0      // new component VolumeValue


ApplicationWindow {
    title: qsTr("Physical values convertor")

    // convert status
    statusBar: StatusBar {
        RowLayout {
            Label {
                id: status
            }
        }
    }

    width: 255
    height: 140

    ColumnLayout {
        x: 10
        y: 10

        RowLayout {
            Label {
                text: qsTr("Physical value:")
            }
            ComboBox {
                id: valuesCombobox
                Layout.fillWidth: true
                model: converter.types
                currentIndex: 2
            }
        }

        // value for convertion
        RowLayout {
            Label {
                text: qsTr("Value:")
            }
            TextField {
                id: valueToConvert
                implicitWidth: 200
                onTextChanged: updateStatusBar()
            }
        }

        // TODO
        // How to dynamically change combobox based on physical value?
        // measures combobox
        RowLayout {
            Label {
                text: qsTr("Measure:")
            }
            ComboBox {
                id: measuresCombobox
                Layout.fillWidth: true
                model: {
                    if (valuesCombobox.currentText == "Massa")
                        mass.measures
                    else if (valuesCombobox.currentText == "Temperature")
                        temp.measures
                    else if (valuesCombobox.currentText == "Volume")
                        vol.measures
                }
                currentIndex: 2
            }
        }

        // result status
        RowLayout {
            Label {
                text: qsTr("Result:")
            }
            Label {
                id: result
                text: {
                    if (valuesCombobox.currentText == "Massa")
                        mass.value
                    else if (valuesCombobox.currentText == "Temperature")
                        temp.value
                    else if (valuesCombobox.currentText == "Volume")
                        vol.value
                }
            }
        }

        // Buttons: Convert, Quit
        RowLayout {
            Button {
                id: convert
                text: qsTr("&Convert")
                onClicked: {
                    if (valuesCombobox.currentText == "Massa")
                        mass.convertValue(valueToConvert.text)
                    else if (valuesCombobox.currentText == "Temperature")
                        temp.convertValue(valueToConvert.text)
                    else if (valuesCombobox.currentText == "Volume")
                        vol.convertValue(valueToConvert.text)
                }
            }
            Button {
                text: qsTr("&Quit")
                onClicked: Qt.quit()
            }
        }
    }

    Converter {
        id: converter
        type: valuesCombobox.currentText
    }

    MassValue {
        id: mass
        measure: measuresCombobox.currentText
    }

    TemperatureValue {
        id: temp
        measure: measuresCombobox.currentText
    }

    VolumeValue {
        id: vol
        measure: measuresCombobox.currentText
    }

    function updateStatusBar() {
        if (valueToConvert.text == "") {
            status.text = qsTr('<font color="red">Enter value for convertion!</font>')
            convert.enabled = false
        } else {
            status.text = ""
            convert.enabled = true
        }
    }

    Component.onCompleted: updateStatusBar()

}
