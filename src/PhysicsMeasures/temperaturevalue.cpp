#include "temperaturevalue.h"

TemperatureValue::TemperatureValue() :
    m_value(0.0), m_measure("C"), m_measures { "C", "F", "K" }
{

}

void TemperatureValue::setMeasure(const QString& _measure)
{
    // Check for valid measure
    if (!m_measures.contains(_measure))
        return;

    if (_measure != m_measure) {
        m_measure = _measure;
        emit measureChanged();
    }
}

double TemperatureValue::convertValue(double _val)
{
    if (m_measure == "C")
        m_value = _val;
    else if (m_measure == "F")
        m_value = _val * 9/5 + 32;  // 1C = 1 * 9/5 + 32; 1F = 1 - 32 * 9/5
    else if (m_measure == "K")
        m_value = _val * 274.15;    // 1C = 274.15 K
    return m_value;
}
