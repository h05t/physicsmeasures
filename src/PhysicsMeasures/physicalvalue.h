#ifndef PHYSICALVALUE
#define PHYSICALVALUE

class PhysicalValue
{
private:
    // double value;
public:
    virtual ~PhysicalValue() {}
    virtual double convertValue(double _val) = 0;

};

Q_DECLARE_INTERFACE(PhysicalValue, "com.h05t.qml.PhysicalValue/1.0")

#endif // PHYSICALVALUE

