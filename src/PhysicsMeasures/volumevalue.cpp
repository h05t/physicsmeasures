#include "volumevalue.h"

VolumeValue::VolumeValue() :
    m_value(0.0), m_measure("Litr"), m_measures { "Litr", "Gallon", "Barrel" }
{

}

void VolumeValue::setMeasure(const QString& _measure)
{
    // Check for valid measure
    if (!m_measures.contains(_measure))
        return;

    if (_measure != m_measure) {
        m_measure = _measure;
        emit measureChanged();
    }
}

double VolumeValue::convertValue(double _val)
{
    if (m_measure == "Litr")
        m_value = _val;
    else if (m_measure == "Gallon")
        m_value = _val * 0.264172;      // 1L = 0,264172G;
    else if (m_measure == "Barrel")
        m_value = _val * 0.00628981;    // 1L = 0,00628981B;
    return m_value;
}
