TEMPLATE = app

QT += qml quick
CONFIG += c++11
# qml_debug

SOURCES += main.cpp \
    massvalue.cpp \
    temperaturevalue.cpp \
    converter.cpp \
    volumevalue.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    physicalvalue.h \
    massvalue.h \
    temperaturevalue.h \
    converter.h \
    volumevalue.h

