#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QObject>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QQuickWindow>
#include <QSurfaceFormat>

#include "converter.h"
#include "massvalue.h"
#include "temperaturevalue.h"
#include "volumevalue.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    // Register our component type with QML.
    qmlRegisterType<Converter>("com.h05t.qml.converter", 1, 0, "Converter");
    qmlRegisterType<MassValue>("com.h05t.qml.mass", 1, 0, "MassValue");
    qmlRegisterType<TemperatureValue>("com.h05t.qml.temp", 1, 0, "TemperatureValue");
    qmlRegisterType<VolumeValue>("com.h05t.qml.volume", 1, 0, "VolumeValue");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

