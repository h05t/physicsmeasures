#include "massvalue.h"

MassValue::MassValue() :
    m_value(0.0), m_measure("Gramm"), m_measures { "Gramm", "Kilo", "Tonna" }
{

}

void MassValue::setMeasure(const QString& _measure)
{
    // Check for valid measure
    if (!m_measures.contains(_measure))
        return;

    if (_measure != m_measure) {
        m_measure = _measure;
        emit measureChanged();
    }
}

double MassValue::convertValue(double _val)
{
    if (m_measure == "Gramm")
        m_value /= _val;
    else if (m_measure == "Kilo")
        m_value = _val / 1000;
    else if (m_measure == "Tonna")
        m_value = _val / 1000000;
    return m_value;
}

