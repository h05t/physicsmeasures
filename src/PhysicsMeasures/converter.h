#ifndef CONVERTER_H
#define CONVERTER_H

#include <QObject>

class Converter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QStringList types READ types NOTIFY typesChanged)
public:
    Converter();
    ~Converter() {}

    QString type() { return m_type; }
    void setType(const QString& _type);

    QStringList types() { return m_types; }

signals:
    void typeChanged();
    void typesChanged();

private:
    QString m_type;
    QStringList m_types;
};

#endif // CONVERTER_H
