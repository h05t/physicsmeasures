#ifndef VOLUMEVALUE_H
#define VOLUMEVALUE_H

#include <QObject>
#include "physicalvalue.h"

class VolumeValue : public QObject, public PhysicalValue
{
    Q_OBJECT
    Q_PROPERTY(double value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QString measure READ measure WRITE setMeasure NOTIFY measureChanged)
    Q_PROPERTY(QStringList measures READ measures NOTIFY measuresChanged)
    Q_INTERFACES(PhysicalValue)

public:
    VolumeValue();
    ~VolumeValue() {}

    double value() { return m_value; }
    void setValue(double _value) { m_value = _value; }

    QString measure() { return m_measure; }
    void setMeasure(const QString& _value);

    QStringList measures() { return m_measures; }

    // Note: Will be accessable in QML!
    //Q_INVOKABLE void addVolumeMeasure(const QString& elem);

public slots:
    virtual double convertValue(double _val);    // convert values

signals:
    void valueChanged();
    void measureChanged();
    void measuresChanged();

private:
    double m_value;
    QString m_measure;
    QStringList m_measures; // measures for mass
};

#endif // VOLUMEVALUE_H
