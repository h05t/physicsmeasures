#ifndef TEMPERATUREVALUE_H
#define TEMPERATUREVALUE_H

#include <QObject>
#include "physicalvalue.h"

class TemperatureValue : public QObject, public PhysicalValue
{
    Q_OBJECT
    Q_PROPERTY(double value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QString measure READ measure WRITE setMeasure NOTIFY measureChanged)
    Q_PROPERTY(QStringList measures READ measures NOTIFY measuresChanged)
    Q_INTERFACES(PhysicalValue)
public:
    TemperatureValue();
    ~TemperatureValue() {}

    double value() { return m_value; }
    void setValue(double _value) { m_value = _value; }

    QString measure() { return m_measure; }
    void setMeasure(const QString& _value);

    QStringList measures() { return m_measures; }

public slots:
    virtual double convertValue(double _val);    // convert values

signals:
    void valueChanged();
    void measureChanged();
    void measuresChanged();

private:
    double m_value;         // current temp value
    QString m_measure;
    QStringList m_measures; // measures for temperature
};

#endif // TEMPERATUREVALUE_H
